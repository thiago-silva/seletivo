package com.processo.seletivo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.processo.seletivo.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	@Query("SELECT u FROM Usuario u "
			+ "WHERE LOWER(u.nome) like '%'||(:nome)||'%' ")
	List<Usuario> findByUsuarioByNome(
			@Param("nome") String nome);
}
