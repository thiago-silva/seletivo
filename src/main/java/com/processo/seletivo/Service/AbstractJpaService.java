package com.processo.seletivo.Service;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;

import com.processo.seletivo.converter.interfaces.Converter;
import com.processo.seletivo.dto.BaseDTO;
import com.processo.seletivo.entity.Persistable;
import com.processo.seletivo.exception.NotFoundException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractJpaService<T extends Persistable<PK>, DTO extends BaseDTO, PK extends Serializable> {
	
	@PersistenceContext
	public EntityManager entityManager;
	
	/**
	 * Repositorio generico
	 * @return
	 */
	protected abstract JpaRepository<T, PK> getRepository();
	
	/**
	 * Conversor
	 * @return
	 */
	protected abstract Converter<T, DTO> getConverter();
	
	/**
	 * Faz a pesquisa utilizando o id que representa a classe
	 * @param id que representa a chave primaria
	 * @return O objeto especifico
	 * @throws Exception 
	 */
	public DTO findById(PK id) {
		log.debug(">> findById [id={}] ", id);
		Optional<T> entity = getRepository().findById(id);
		log.debug("<< findById [id={}, entity={}] ", id, entity);
		Optional<DTO> dto = entity.isPresent() ? Optional.of(getConverter().convertToDTO(entity.get())) : Optional.empty();
		log.debug("<< findById [id={}, entity={}, dto={}] ", id, entity, dto);
		return dto.orElseThrow(() -> new NotFoundException());
	}
	
	/**
	 * Salva uma nova entidade no banco
	 * @param entity a entidade generica a ser criada
	 * @return a entity salva no banco
	 */
	public DTO save(DTO dto) {
		log.debug(">> save [dto={}] ", dto);
		T entity = getConverter().convertToEntity(dto);
		log.debug(">> save [entity={}, dto={}] ", entity, dto);
		T t = getRepository().save(entity);
		log.debug("<< save [entity={}] ", t);
		dto =  getConverter().convertToDTO(t);
		log.debug("<< save [dto={}] ", dto);
		return dto;
	}
	
	/**
	 * Atualiza uma determinada entidade
	 * @param id o id da entidade a ser atualizada
	 * @param entity a entidade com os dados a ser atualizado
	 * @return a entidade atualizada
	 */
	public DTO update(PK id, DTO entity) {
		log.debug(">> update [entity={}] ", entity);
		DTO dto = findById(id);
		log.debug("<< update [dto={}] ", dto);
		dto = getConverter().convertToClone(entity, dto);
		log.debug("<< update [dto={}] ", dto);
		T t = getRepository().save(getConverter().convertToEntity(dto));
		log.debug("<< update [entity={}] ", t);
		entity = getConverter().convertToDTO(t);
		log.debug("<< update [entity={}] ", entity);
		return entity;
	}
	
	/**
	 * Exclua uma entidade cujo id exista no banco
	 * @param id
	 */
	public void delete(PK id) {
		log.debug(">> delete [id={}] ", id);
		getRepository().deleteById(id);
	}
	
}
