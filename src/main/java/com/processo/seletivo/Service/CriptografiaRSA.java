package com.processo.seletivo.Service;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class CriptografiaRSA {
	
	private static final String ALGORITHM = "RSA";
	private static final Integer SIZE_ALGORITHM = 2048;
	
	private CriptografiaRSA() {}
	
	public static KeyPair gerarChave() throws NoSuchAlgorithmException {
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
		keyGen.initialize(SIZE_ALGORITHM);
		return keyGen.generateKeyPair();
	}
	
	public static RSAPublicKeySpec rsaKeyPublic(PublicKey chavePublica) throws InvalidKeySpecException, NoSuchAlgorithmException {
		KeyFactory fact = KeyFactory.getInstance(ALGORITHM);
		return fact.getKeySpec(chavePublica, RSAPublicKeySpec.class);
	}
	
	public static RSAPrivateKeySpec rsaKeyPrivate(PrivateKey chavePublica) throws InvalidKeySpecException, NoSuchAlgorithmException {
		KeyFactory fact = KeyFactory.getInstance(ALGORITHM);
		return fact.getKeySpec(chavePublica, RSAPrivateKeySpec.class);
	}
	
	public static PublicKey convertStringToPublicKey(String chave) throws InvalidKeySpecException, NoSuchAlgorithmException {
		String[] string = chave.split(";");
		return KeyFactory.getInstance(ALGORITHM)
				.generatePublic(new RSAPublicKeySpec(
						new BigInteger(string[0]), 
						new BigInteger(string[1])));
	}
	
	public static PrivateKey convertStringToPrivateKey(String chave) throws InvalidKeySpecException, NoSuchAlgorithmException {
		String[] string = chave.split(";");
		return KeyFactory.getInstance(ALGORITHM)
				.generatePrivate(new RSAPrivateKeySpec(
						new BigInteger(string[0]),
						new BigInteger(string[1])));
	}
	
	public static byte[] criptografar(String texto, PublicKey chave) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, chave);
		return cipher.doFinal(texto.getBytes());
	}
	
	public static String descriptografar(byte[] texto, PrivateKey chave) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, chave);
		return new String(cipher.doFinal(texto));
	}
}
