package com.processo.seletivo.Service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.processo.seletivo.Repository.UsuarioRepository;
import com.processo.seletivo.converter.UsuarioConverter;
import com.processo.seletivo.converter.interfaces.Converter;
import com.processo.seletivo.dto.ChavesCriptografadasUsuarioDTO;
import com.processo.seletivo.dto.DigitoUnicoDTO;
import com.processo.seletivo.dto.UsuarioCriptografadoDTO;
import com.processo.seletivo.dto.UsuarioDTO;
import com.processo.seletivo.dto.UsuarioDescriptografadoDTO;
import com.processo.seletivo.entity.Usuario;
import com.processo.seletivo.exception.SingleDigitExeption;
import com.processo.seletivo.exception.UserNotFoundExeption;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
public class UsuarioService extends AbstractJpaService<Usuario, UsuarioDTO, Long> {

	@Autowired
	private UsuarioRepository repository;
	@Autowired
	private UsuarioConverter converter;

	//Atributo para salvar os ultimos 10 calculos na memória
	private List<DigitoUnicoDTO> digitoUnicoDTOs = new ArrayList<>();
	
	public ChavesCriptografadasUsuarioDTO gerarChavesCriptografadasUsuario(UsuarioDTO dto) throws NoSuchAlgorithmException, InvalidKeySpecException {
		log.info(">> gerarChavesCriptografadasUsuario [dto={}]", dto);
		return new ChavesCriptografadasUsuarioDTO(dto, 
				CriptografiaRSA.rsaKeyPublic(CriptografiaRSA.gerarChave().getPublic()).getModulus().toString() + ";" + CriptografiaRSA.rsaKeyPublic(CriptografiaRSA.gerarChave().getPublic()).getPublicExponent().toString(),
				CriptografiaRSA.rsaKeyPrivate(CriptografiaRSA.gerarChave().getPrivate()).getModulus().toString() + ";" + CriptografiaRSA.rsaKeyPrivate(CriptografiaRSA.gerarChave().getPrivate()).getPrivateExponent().toString());
	}
	
	public UsuarioCriptografadoDTO criptografarUsuario(Long id, String chavePublica) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
		log.info(">> criptografarUsuario [id={}, chavePublica={}]", id, chavePublica);
		UsuarioDTO dto = findById(id);
		log.info("<< criptografarUsuario [dto={}]", dto);
		return new UsuarioCriptografadoDTO(CriptografiaRSA.criptografar(dto.getNome() + " " + dto.getEmail(), 
				CriptografiaRSA.convertStringToPublicKey(chavePublica)));
	}
	
	public UsuarioDescriptografadoDTO descriptografarUsuario(byte[] usuarioCriptografado, String chavePrivada) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException {
		log.info(">> descriptografarUsuario [usuarioCriptografado={}, chavePrivada={}]", usuarioCriptografado, chavePrivada);
		return new UsuarioDescriptografadoDTO(CriptografiaRSA
				.descriptografar(usuarioCriptografado, 
						CriptografiaRSA.convertStringToPrivateKey(chavePrivada)));
	}

	public List<UsuarioDTO> findUsuarioByNome(String nome) {
		log.info(">> findUsuarioByNomeOrEmail [nome={}]", nome);
		List<UsuarioDTO> dtos = repository.findByUsuarioByNome(nome.toLowerCase()).stream()
				.map(user -> getConverter().convertToDTO(user)).collect(Collectors.toList());
		log.info("<< findUsuarioByNomeOrEmail [dtos={}]", dtos.size());
		if (CollectionUtils.isEmpty(dtos)) {
			throw new UserNotFoundExeption();
		}
		dtos.stream().forEach(dt -> {
			dt.setDigitoUnicos(this.digitoUnicoDTOs.stream()
					.filter(item -> Objects.equals(item.getUsuario(), dt.getId()))
					.map(result -> result)
					.collect(Collectors.toList()));});
		log.info("<< findUsuarioByNomeOrEmail [dtos={}]", dtos.size());
		return dtos;
	}
	
	public List<DigitoUnicoDTO> findDigitoUnicoDTOAll(){
		log.info(">> findDigitoUnicoDTOAll");
		List<DigitoUnicoDTO> dtos = this.digitoUnicoDTOs;
		log.info("<< findDigitoUnicoDTOAll [dtos={}]", dtos.size());
		return dtos;
	}

	public DigitoUnicoDTO calculoDigitoUnicoAndSave(DigitoUnicoDTO dto) {
		log.info(">> saveAndCalculeDigitoUnico [dto={}]", dto);
		if (CollectionUtils.isNotEmpty(this.digitoUnicoDTOs) && calculoExisteNoCache(dto)) {
			throw new SingleDigitExeption();
		}
		Integer resultado = calcular(dto);
		log.info("<< saveAndCalculeDigitoUnico [resultado={}]", resultado);
		atualizarUltimosCalculos(dto, resultado);
		log.info("<< saveAndCalculeDigitoUnico [digitoUnicoDTOs={}]", this.digitoUnicoDTOs);
		return new DigitoUnicoDTO(dto.getUsuario(), dto.getEntradaN(), dto.getEntradaK(), resultado);
	}

	private void atualizarUltimosCalculos(DigitoUnicoDTO dto, Integer resultado) {
		if(Objects.equals(this.digitoUnicoDTOs.size(), 10)) {
			this.digitoUnicoDTOs.remove(0);
			this.digitoUnicoDTOs.add(new DigitoUnicoDTO(dto.getUsuario(), dto.getEntradaN(), dto.getEntradaK(), resultado));
		} else {
			this.digitoUnicoDTOs.add(new DigitoUnicoDTO(dto.getUsuario(), dto.getEntradaN(), dto.getEntradaK(), resultado));			
		}
	}

	private Integer calcular(DigitoUnicoDTO dto) {
		Integer resultado = 0;
		StringBuilder builder = new StringBuilder();
		boolean next = true;
		if (Objects.nonNull(dto.getEntradaN()) && dto.getEntradaN() >= 1 && dto.getEntradaN() <= 1000000
				&& Objects.nonNull(dto.getEntradaK()) && dto.getEntradaK() >= 1 && dto.getEntradaK() <= 100000) {
			Integer entradaNAux = dto.getEntradaN();
			Integer entradaKAux = dto.getEntradaK();
			do {
				builder.append(entradaNAux.toString());
				entradaKAux--;
				if (entradaKAux <= 0) {
					for (int i = 0; i < builder.length(); i++) {
						resultado += Integer.parseInt(builder.substring(i, i + 1));
					}
				}
				if (resultado > 9) {
					entradaNAux = resultado;
					resultado = 0;
					builder = new StringBuilder();
				} else if (resultado < 10 && entradaKAux <= 0) {
					next = false;
				}
			} while (next);		
		}
		return resultado;
	}

	private boolean calculoExisteNoCache(DigitoUnicoDTO dto) {
		return this.digitoUnicoDTOs.stream()
				.filter(item -> Objects.equals(item.getEntradaN(), dto.getEntradaN())
						&& Objects.equals(item.getEntradaK(), dto.getEntradaK()))
				.map(result -> result).findAny().isPresent();
	}

	@Override
	protected JpaRepository<Usuario, Long> getRepository() {
		return repository;
	}

	@Override
	protected Converter<Usuario, UsuarioDTO> getConverter() {
		return converter;
	}

}
