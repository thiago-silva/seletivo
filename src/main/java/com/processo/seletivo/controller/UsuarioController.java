package com.processo.seletivo.controller;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.processo.seletivo.Service.UsuarioService;
import com.processo.seletivo.dto.ChavesCriptografadasUsuarioDTO;
import com.processo.seletivo.dto.DigitoUnicoDTO;
import com.processo.seletivo.dto.UsuarioCriptografadoDTO;
import com.processo.seletivo.dto.UsuarioDTO;
import com.processo.seletivo.dto.UsuarioDescriptografadoDTO;
import com.processo.seletivo.resource.UsuarioResource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController implements UsuarioResource {

	@Autowired
	private UsuarioService service;
	
	@Override
	public List<UsuarioDTO> getUsuario(String nome) {
		log.info(">> getUsuario [nome={}] ", nome);
		List<UsuarioDTO> dtos = service.findUsuarioByNome(nome);
		log.info("<< getUsuario [dtos={}] ", dtos.size());
		return dtos;
	}

	@Override
	public ChavesCriptografadasUsuarioDTO saveUsuario(UsuarioDTO dto) throws NoSuchAlgorithmException, InvalidKeySpecException {
		log.info(">> saveUsuario [dto={}] ", dto);
		UsuarioDTO usuario = service.save(dto);
		log.info("<< saveUsuario [usuario={}] ", usuario);
		return service.gerarChavesCriptografadasUsuario(usuario);
	}

	@Override
	public UsuarioDTO updateUsuario(@Valid UsuarioDTO dto, Long id) {
		log.info(">> updateUsuario [dto={}, id={}] ", dto, id);
		UsuarioDTO dtoSave = service.update(id, dto);
		log.info("<< updateUsuario [dtoSave={}] ", dtoSave);
		return dtoSave;
	}

	@Override
	public void deleteUsuario(Long id) {
		log.info(">> deleteUsuario [id={}] ", id);
		service.delete(id);
	}

	@Override
	public DigitoUnicoDTO saveCalculoDigitoUnico(DigitoUnicoDTO dto) {
		log.info(">> saveCalculoDigitoUnico [dto={}] ", dto);
		DigitoUnicoDTO dtoSave = service.calculoDigitoUnicoAndSave(dto);
		log.info("<< saveCalculoDigitoUnico [dtoSave={}] ", dtoSave);
		return dtoSave;
	}

	@Override
	public List<DigitoUnicoDTO> getDigitoUnico() {
		log.info(">> getDigitoUnico");
		List<DigitoUnicoDTO> dtos = service.findDigitoUnicoDTOAll();
		log.info("<< getDigitoUnico [dtos={}] ", dtos.size());
		return dtos;
	}

	@Override
	public UsuarioCriptografadoDTO enviarChavePublicaUsuario(Long id, String chavePublica) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
		log.info(">> enviarChavePublicaUsuario [id={}, chavePublica={}] ", id, chavePublica);
		UsuarioCriptografadoDTO dto = service.criptografarUsuario(id, chavePublica);
		log.info("<< enviarChavePublicaUsuario [dto={}] ", dto);
		return dto;
	}

	@Override
	public UsuarioDescriptografadoDTO enviarChavePrivadaUsuario(byte[] usuarioCriptografado, String chavePrivada) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException {
		log.info(">> enviarChavePublicaUsuario [usuarioCriptografado={}, chavePrivada={}] ", usuarioCriptografado, chavePrivada);
		UsuarioDescriptografadoDTO dto = service.descriptografarUsuario(usuarioCriptografado, chavePrivada);
		log.info("<< enviarChavePublicaUsuario [dto={}] ", dto);
		return dto;
	}

}
