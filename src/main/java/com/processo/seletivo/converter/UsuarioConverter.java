package com.processo.seletivo.converter;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.processo.seletivo.converter.interfaces.Converter;
import com.processo.seletivo.dto.UsuarioDTO;
import com.processo.seletivo.entity.Usuario;

@Component
public class UsuarioConverter implements Converter<Usuario, UsuarioDTO> {

	@Override
	public Usuario convertToEntity(UsuarioDTO dto) {
		Usuario usuario = new Usuario();
		BeanUtils.copyProperties(dto, usuario);
		return usuario;
	}

	@Override
	public UsuarioDTO convertToDTO(Usuario entity) {
		UsuarioDTO dto = new UsuarioDTO();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

}
