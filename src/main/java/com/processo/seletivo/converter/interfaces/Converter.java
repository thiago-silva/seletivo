package com.processo.seletivo.converter.interfaces;

import org.springframework.beans.BeanUtils;

public interface Converter<T, DTO> {

	T convertToEntity(DTO dto);

	/**
	 * Converter de um dto para uma entidade
	 * 
	 * @param entity converte a partir de uma entidade para um dto
	 * @return
	 */
	public DTO convertToDTO(T entity);

	/**
	 * Converter de um dto para um outro dto utilizado no update
	 * 
	 * @param source objeto de origem
	 * @param target objeto de destino
	 * @return
	 */
	public default DTO convertToClone(DTO source, DTO target) {
		BeanUtils.copyProperties(source, target, "id");
		return target;
	}

}
