package com.processo.seletivo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChavesCriptografadasUsuarioDTO {
	private UsuarioDTO usuarioDTO;
	private String chavePublica;
	private String chavePrivada;
}
