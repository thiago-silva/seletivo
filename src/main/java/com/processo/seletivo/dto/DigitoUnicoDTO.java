package com.processo.seletivo.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class DigitoUnicoDTO implements Serializable {
	private Long usuario;
	@NotNull
	private Integer entradaN;
	@NotNull
	private Integer entradaK;
	private Integer resultado;
}
