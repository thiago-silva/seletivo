package com.processo.seletivo.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class UsuarioDTO extends BaseDTO {
	private Long id;
	@NotEmpty
	@Size(min = 3, max = 30)
	private String nome;
	@NotEmpty
	@Size(min = 3, max = 30)
	private String email;
	private List<DigitoUnicoDTO> digitoUnicos;
}
