package com.processo.seletivo.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class UsuarioDescriptografadoDTO implements Serializable {
	private String usuarioDescriptografado;
}
