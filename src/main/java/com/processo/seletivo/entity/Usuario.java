package com.processo.seletivo.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.processo.seletivo.dto.DigitoUnicoDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "usuario")
public class Usuario implements Persistable<Long> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_id_seq")
	@SequenceGenerator(name = "usuario_id_seq", sequenceName = "usuario_id_seq", allocationSize = 1, initialValue = 1)
	@Column(name = "id")
	private Long id;
	
	@NotEmpty
	@Size(min = 3, max = 30)
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@NotEmpty
	@Size(min = 3, max = 30)
	@Column(name = "email", nullable = false)
	private String email;
	
	@Transient
	private List<DigitoUnicoDTO> digitoUnicos;
	
}
