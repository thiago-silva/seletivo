package com.processo.seletivo.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.IllegalBlockSizeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class SeletivoHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	@SuppressWarnings("unused")
	private static ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<Erro> erros = criarListaDeErros(ex.getBindingResult());
		return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
	}
	
	private List<Erro> criarListaDeErros(BindingResult bindingResult){
		List<Erro> erros = new ArrayList<>();
		for(FieldError fildError : bindingResult.getFieldErrors()) {			
			String mensagemUsuario = messageSource.getMessage(fildError, LocaleContextHolder.getLocale());
			String mensagemDesenvolvedor = fildError.toString();
			erros.add(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		}
		return erros;
	}
	
	@ExceptionHandler({UserNotFoundExeption.class})
	public ResponseEntity<Object> handleUserNotFoundExeption(UserNotFoundExeption ex){
		String mensagemUsuario = messageSource.getMessage("usuario-nao-existe", null, LocaleContextHolder.getLocale());
		log.info("<< handleUserNotFoundExeption [mensagemUsuario={}] ", mensagemUsuario);
		String mensagemDesenvolvedor = ex.toString();
		log.info("<< handleUserNotFoundExeption [mensagemDesenvolvedor={}] ", mensagemDesenvolvedor);
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		log.info("<< handleUserNotFoundExeption [erros={}] ", erros);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erros);
	}
	
	@ExceptionHandler({SingleDigitExeption.class})
	public ResponseEntity<Object> handleSingleDigitExeption(SingleDigitExeption ex){
		String mensagemUsuario = messageSource.getMessage("digito-unico-existe", null, LocaleContextHolder.getLocale());
		log.info("<< handleSingleDigitExeption [mensagemUsuario={}] ", mensagemUsuario);
		String mensagemDesenvolvedor = ex.toString();
		log.info("<< handleSingleDigitExeption [mensagemDesenvolvedor={}] ", mensagemDesenvolvedor);
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		log.info("<< handleSingleDigitExeption [erros={}] ", erros);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erros);
	}
	
	@ExceptionHandler({IllegalBlockSizeException.class})
	public ResponseEntity<Object> handleIllegalBlockSizeException(IllegalBlockSizeException ex){
		String mensagemUsuario = messageSource.getMessage("dados-tamanho--bytes", null, LocaleContextHolder.getLocale());
		log.info("<< handleIllegalArgumentException [mensagemUsuario={}] ", mensagemUsuario);
		String mensagemDesenvolvedor = ex.toString();
		log.info("<< handleIllegalArgumentException [mensagemDesenvolvedor={}] ", mensagemDesenvolvedor);
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		log.info("<< handleIllegalArgumentException [erros={}] ", erros);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erros);
	}
	
	@ExceptionHandler({NotFoundException.class})
	public ResponseEntity<Object> handleNotFoundException(NotFoundException ex){
		String mensagemUsuario = messageSource.getMessage("nada-encontrado", null, LocaleContextHolder.getLocale());
		log.info("<< handleIllegalArgumentException [mensagemUsuario={}] ", mensagemUsuario);
		String mensagemDesenvolvedor = ex.toString();
		log.info("<< handleIllegalArgumentException [mensagemDesenvolvedor={}] ", mensagemDesenvolvedor);
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		log.info("<< handleIllegalArgumentException [erros={}] ", erros);
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erros);
	}
	
	@ExceptionHandler({IllegalArgumentException.class})
	public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex){
		String mensagemUsuario = messageSource.getMessage("parametros-obrigatorios", null, LocaleContextHolder.getLocale());
		log.info("<< handleIllegalArgumentException [mensagemUsuario={}] ", mensagemUsuario);
		String mensagemDesenvolvedor = ex.toString();
		log.info("<< handleIllegalArgumentException [mensagemDesenvolvedor={}] ", mensagemDesenvolvedor);
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		log.info("<< handleIllegalArgumentException [erros={}] ", erros);
		return ResponseEntity.badRequest().body(erros);
	}
	
	@ExceptionHandler({EmptyResultDataAccessException.class})
	public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex){
		String mensagemUsuario = messageSource.getMessage("nada-encontrado-nenhuma-acao-executada", null, LocaleContextHolder.getLocale());
		log.info("<< handleIllegalArgumentException [mensagemUsuario={}] ", mensagemUsuario);
		String mensagemDesenvolvedor = ex.toString();
		log.info("<< handleIllegalArgumentException [mensagemDesenvolvedor={}] ", mensagemDesenvolvedor);
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		log.info("<< handleIllegalArgumentException [erros={}] ", erros);
		return ResponseEntity.badRequest().body(erros);
	}
	
	public static class Erro{
		private String mensagemUsuario;
		private String mensagemDesenvolvedor;
		
		public Erro(String mensagemUsuario, String mensagemDesenvolvedor) {
			this.mensagemUsuario = mensagemUsuario;
			this.mensagemDesenvolvedor = mensagemDesenvolvedor;
		}

		public String getMensagemUsuario() {
			return mensagemUsuario;
		}

		public void setMensagemUsuario(String mensagemUsuario) {
			this.mensagemUsuario = mensagemUsuario;
		}

		public String getMensagemDesenvolvedor() {
			return mensagemDesenvolvedor;
		}

		public void setMensagemDesenvolvedor(String mensagemDesenvolvedor) {
			this.mensagemDesenvolvedor = mensagemDesenvolvedor;
		}	
	}

}
