package com.processo.seletivo.resource;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.processo.seletivo.dto.ChavesCriptografadasUsuarioDTO;
import com.processo.seletivo.dto.DigitoUnicoDTO;
import com.processo.seletivo.dto.UsuarioCriptografadoDTO;
import com.processo.seletivo.dto.UsuarioDTO;
import com.processo.seletivo.dto.UsuarioDescriptografadoDTO;

public interface UsuarioResource {
	
	@GetMapping
	public List<UsuarioDTO> getUsuario(@RequestParam(name = "nome", required = true) String nome);
	
	@PostMapping
	public ChavesCriptografadasUsuarioDTO saveUsuario(@Valid @RequestBody UsuarioDTO dto) throws NoSuchAlgorithmException, InvalidKeySpecException;
	
	@PutMapping
	public UsuarioDTO updateUsuario(@Valid @RequestBody UsuarioDTO dto, 
			@RequestParam(name = "id_usuario", required = true) Long id);
	
	@DeleteMapping
	public void deleteUsuario(@RequestParam(name = "id_usuario", required = true) Long id);
	
	@PostMapping(path = "/calculo-digito-unico")
	public DigitoUnicoDTO saveCalculoDigitoUnico(@RequestBody DigitoUnicoDTO dto);
	
	@GetMapping(path = "/digito-unico")
	public List<DigitoUnicoDTO> getDigitoUnico();
	
	@PostMapping(path = "/chave-public-criptografia")
	public UsuarioCriptografadoDTO enviarChavePublicaUsuario(
			@RequestParam("id_usuario") Long id, 
			@RequestParam("chave_publica") String chavePublica) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException;

	@PostMapping(path = "/chave-privada-descriptografia")
	public UsuarioDescriptografadoDTO enviarChavePrivadaUsuario(
			@RequestParam("usuario_criptografado") byte[] usuarioCriptografado, 
			@RequestParam("chave_privada") String chavePrivada) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException;
	
}
