package com.processo.seletivo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.processo.seletivo.Service.UsuarioService;
import com.processo.seletivo.dto.ChavesCriptografadasUsuarioDTO;
import com.processo.seletivo.dto.DigitoUnicoDTO;
import com.processo.seletivo.dto.UsuarioDTO;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
class SeletivoApplicationTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private UsuarioService usuarioService;

	@Test
	@Order(1)
	void salvarUsuarioCaseTest1() throws JsonProcessingException, Exception {
		mockMvc.perform(post("/api/usuarios")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(new UsuarioDTO(null, "Nome", "Email", new ArrayList<>()))))
				.andExpect(status().isOk());
		
		ChavesCriptografadasUsuarioDTO dto = usuarioService.gerarChavesCriptografadasUsuario(new UsuarioDTO(null, "Nome", "Email", new ArrayList<>()));
		
		Assertions.assertThat(dto.getUsuarioDTO()).isNotNull();
		Assertions.assertThat(dto.getChavePublica()).isNotBlank();
		Assertions.assertThat(dto.getChavePrivada()).isNotBlank();
	}
	
	@Test
	@Order(2)
	void getUsuarioCaseTest2() throws JsonProcessingException, Exception {
		mockMvc.perform(get("/api/usuarios")
				.contentType(MediaType.APPLICATION_JSON)
				.param("nome", "nome"))
				.andExpect(status().isOk());
		
		List<UsuarioDTO> dtos = usuarioService.findUsuarioByNome("nome");
		
		Assertions.assertThat(dtos).isNotEmpty();
	}
	
	@Test
	@Order(3)
	void saveCalculoDigitoUnicoCaseTest3() throws JsonProcessingException, Exception {
		mockMvc.perform(post("/api/usuarios/calculo-digito-unico")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(new DigitoUnicoDTO(null, 15, 3, null))))
				.andExpect(status().isOk());
	}
	
	@Test
	@Order(4)
	void updateUsuarioCaseTest4() throws JsonProcessingException, Exception {
		Long id = (long) 1;
		mockMvc.perform(put("/api/usuarios?id_usuario="+id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(new UsuarioDTO(null, "Nome Sobrenome", "Email", new ArrayList<>()))))
				.andExpect(status().isOk());
		
		UsuarioDTO dto = usuarioService.update(id, new UsuarioDTO(null, "Nome Sobrenome", "Email novo", new ArrayList<>()));
		
		Assertions.assertThat(dto.getId()).isNotNull();
		Assertions.assertThat(dto.getNome()).isEqualTo("Nome Sobrenome");
		Assertions.assertThat(dto.getEmail()).isEqualTo("Email novo");
	}
	
	@Test
	@Order(5)
	void deleteUsuarioCaseTest5() throws JsonProcessingException, Exception {
		Long id = (long) 1;
		mockMvc.perform(delete("/api/usuarios?id_usuario="+id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

}
